﻿using LocalizationModule.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LocalizationModule.Services
{
    public class ElasticServiceAgent : NotifyPropertyChange
    {
        public clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderLoclizedToRef;
        private OntologyModDBConnector dbReaderMessagesOfLocalization;
        private OntologyModDBConnector dbReaderLanguagesOfLocalization;
        private OntologyModDBConnector dbReaderLanguages;
        private OntologyModDBConnector dbReaderStandardLanguages;
        private OntologyModDBConnector dbReaderOItem;

        private Thread getLocalizedDataAsync;

        private clsOntologyItem oItemRef;
        private LanguageItem languageItem;

        private clsOntologyItem resultLocalizedData;
        public clsOntologyItem ResultLocalizedData
        {
            get { return resultLocalizedData; }
            set
            {
                resultLocalizedData = value;
                RaisePropertyChanged(Notifications.NotifyChanges.ElasticServiceAgent_ResultLocalizedData);
            }
        }

        public List<clsObjectRel> LocalizedToRefList
        {
            get { return dbReaderLoclizedToRef.ObjectRels; }
        }

        public List<clsObjectAtt> MessagesOfLocalizationList
        {
            get { return dbReaderMessagesOfLocalization.ObjAtts; }
        }

        public List<clsObjectRel> LanguagesOfLoclizationList
        {
            get { return dbReaderLanguagesOfLocalization.ObjectRels; }
        }

        public List<clsOntologyItem> StandardLanguages
        {
            get; private set;
        }

        public clsOntologyItem GetLocalizedData(clsOntologyItem oItemRef)
        {
            this.oItemRef = oItemRef;

            StopRead();

            getLocalizedDataAsync = new Thread(GetLocalizedDataAsync);
            getLocalizedDataAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetLocalizedDataAsync()
        {
            var result = GetSub_001_LocalizedDescriptionsToRef();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = result;
                return;
            }

            result = GetSub_002_MessageOfLocalization();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = result;
                return;
            }

            result = GetSub_003_LanguagesOfLocalization();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = result;
                return;
            }

            ResultLocalizedData = result;
        }

        public clsOntologyItem GetData_Languages()
        {
            clsOntologyItem objOItem_Result = localConfig.Globals.LState_Success.Clone();

            var objOL_Languages = new List<clsOntologyItem> { new clsOntologyItem { GUID_Parent = localConfig.OItem_type_language.GUID } };

            objOItem_Result = dbReaderLanguages.GetDataObjects(objOL_Languages);

            return objOItem_Result;
        }

        public clsOntologyItem GetOItem(string idItem, string type)
        {
            return dbReaderOItem.GetOItem(idItem, type);
        }

        public clsOntologyItem GetStandardLanguages()
        {
            var objORel_Languages = new List<clsObjectRel> { new clsObjectRel {ID_Object = localConfig.OItem_object_base_config.GUID,
            ID_RelationType = localConfig.OItem_relationtype_offers.GUID,
            ID_Parent_Other = localConfig.OItem_type_language.GUID } };

            var result = dbReaderStandardLanguages.GetDataObjectRel(objORel_Languages, doIds: false);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dbReaderStandardLanguages.ObjectRels.Any())
                {
                    StandardLanguages = dbReaderStandardLanguages.ObjectRels.Select(sl => new clsOntologyItem
                    {
                        GUID = sl.ID_Other,
                        Name = sl.Name_Other,
                        GUID_Parent = sl.ID_Parent_Other,
                        Type = localConfig.Globals.Type_Object
                    }).Where(l => l.GUID != localConfig.OItem_StandardLanguage.GUID).ToList();


                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }
            }
            else
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        private clsOntologyItem GetSub_001_LocalizedDescriptionsToRef()
        {
            var searchLocalizationsToRef = new List<clsObjectRel> { new clsObjectRel {ID_Other = oItemRef.GUID,
                ID_Parent_Object = localConfig.OItem_type_localizeddescription.GUID,
                ID_RelationType = localConfig.OItem_relationtype_describes.GUID } };

            searchLocalizationsToRef.Add(new clsObjectRel
            {
                ID_Other = oItemRef.GUID,
                ID_Parent_Object = localConfig.OItem_type_localized_names.GUID,
                ID_RelationType = localConfig.OItem_relationtype_alternative_for.GUID
            });

            var result = dbReaderLoclizedToRef.GetDataObjectRel(searchLocalizationsToRef);

            return result;
        }

        private clsOntologyItem GetSub_002_MessageOfLocalization()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var objORList_LocalizationToRef = dbReaderLoclizedToRef.ObjectRels.Where(loc => loc.ID_Parent_Object == localConfig.OItem_type_localizeddescription.GUID).ToList();
            if (objORList_LocalizationToRef.Any())
            {
                var objOARel_Localization__Message = dbReaderLoclizedToRef.ObjectRels.Select(loc => new clsObjectAtt
                {
                    ID_Object = loc.ID_Object,
                    ID_AttributeType = localConfig.OItem_attributetype_message.GUID
                }).ToList();

                result = dbReaderMessagesOfLocalization.GetDataObjectAtt(objOARel_Localization__Message, doIds: false);
            }
            else
            {
                dbReaderMessagesOfLocalization.ObjAtts.Clear();
            }


            return result;
        }

        private clsOntologyItem GetSub_003_LanguagesOfLocalization()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            if (dbReaderLoclizedToRef.ObjectRels.Any())
            {
                var objOLRel_LocalizationToLanguage = dbReaderLoclizedToRef.ObjectRels.Select(loc => new clsObjectRel
                {
                    ID_Object = loc.ID_Object,
                    ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                    ID_Parent_Other = localConfig.OItem_type_language.GUID
                }).ToList();

                result = dbReaderLanguagesOfLocalization.GetDataObjectRel(objOLRel_LocalizationToLanguage, doIds: false);
            }
            else
            {
                dbReaderLanguagesOfLocalization.ObjectRels.Clear();
            }



            return result;
        }

        public clsOntologyItem GetLocalizedDescriptionOfReference(clsOntologyItem oItemReference, LanguageItem languageItem)
        {
            oItemRef = oItemReference;
            this.languageItem = languageItem;

            StopRead();

            getLocalizedDataAsync = new Thread(GetLocalizedDataOfReferenceAsync);
            getLocalizedDataAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void GetLocalizedDataOfReferenceAsync()
        {
            var result = GetSub_001_RefToLocalized();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = localConfig.Globals.LState_Error.Clone();
                return;
            }

            result = GetSub_002_LocalizedToLanguage();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = localConfig.Globals.LState_Error.Clone();
                return;
            }

            result = GetSub_003_LocalizedToMessage();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ResultLocalizedData = localConfig.Globals.LState_Error.Clone();
                return;
            }

            ResultLocalizedData = result;

        }

        private clsOntologyItem GetSub_001_RefToLocalized()
        {
            var searchLocalizedDescriptions = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_describes.GUID,
                    ID_Parent_Object = localConfig.OItem_type_localizeddescription.GUID
                }
            };

            var result = dbReaderLoclizedToRef.GetDataObjectRel(searchLocalizedDescriptions);

            return result;
        }

        private clsOntologyItem GetSub_002_LocalizedToLanguage()
        {
            var searchLocalizedDescription = dbReaderLoclizedToRef.ObjectRels.Select(locLang => new clsObjectRel
            {
                ID_Object = locLang.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                ID_Other = languageItem.IdLanguage
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchLocalizedDescription.Any())
            {
                result = dbReaderLanguagesOfLocalization.GetDataObjectRel(searchLocalizedDescription);
            }
            else
            {
                dbReaderLanguagesOfLocalization.ObjectRels.Clear();
            }

            

            return result;
        }

        private clsOntologyItem GetSub_003_LocalizedToMessage()
        {
            var searchLocalizedMessage = dbReaderLoclizedToRef.ObjectRels.Select(locLang => new clsObjectAtt
            {
                ID_Object = locLang.ID_Object,
                ID_AttributeType = localConfig.OItem_attributetype_message.GUID,
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();

            if (searchLocalizedMessage.Any())
            {
                result = dbReaderMessagesOfLocalization.GetDataObjectAtt(searchLocalizedMessage);
            }
            else
            {
                dbReaderMessagesOfLocalization.ObjAtts.Clear();
            }

            return result;
        }

        public void StopRead()
        {
            if (getLocalizedDataAsync != null)
            {
                try
                {
                    getLocalizedDataAsync.Abort();
                }
                catch(Exception ex)
                {
                    
                }
            }

        }

        public ElasticServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbReaderLoclizedToRef = new OntologyModDBConnector(localConfig.Globals);
            dbReaderMessagesOfLocalization = new OntologyModDBConnector(localConfig.Globals);
            dbReaderLanguagesOfLocalization = new OntologyModDBConnector(localConfig.Globals);
            dbReaderLanguages = new OntologyModDBConnector(localConfig.Globals);
            dbReaderStandardLanguages = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);
        }

    }
}
