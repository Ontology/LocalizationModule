﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule
{
    public class MessageItem
    {
        public string IdMessage { get; set; }
        public string NameMessage { get; set; }
        public string IdLocalizedMessage { get; set; }
        public string NameLocalizedMessage { get; set; }
        public string IdLanguage { get; set; }
        public string NameLanguage { get; set; }
        public string LanguageShort { get; set; }
        public string IdRelationType { get; set; }
        public string NameRelationType { get; set; }
        public string Message { get; set; }
        public string Caption { get; set; }
        
        
    }
}
