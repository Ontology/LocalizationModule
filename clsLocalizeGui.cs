﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using System.Globalization;
using OntologyAppDBConnector;

namespace LocalizationModule
{
    public class clsLocalizeGui
    {
        
        private IGuiLocalization localConfig;

        private OntologyModDBConnector objDBLevel_GuiItems_L1;
        private OntologyModDBConnector objDBLevel_GuiItems_L2;
        private OntologyModDBConnector objDBLevel_GuiCaption;
        private OntologyModDBConnector objDBLevel_ToolTips;
        private OntologyModDBConnector objDBLevel_Messages;
        private OntologyModDBConnector objDBLevel_LocalizedMessages;
        private OntologyModDBConnector objDBLevel_Languages;
        private OntologyModDBConnector objDBLevel_LanguageShort;
        private OntologyModDBConnector objDBLevel_Caption;
        private OntologyModDBConnector objDBLevel_Message;

        public clsLocalizeGui(IGuiLocalization localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        public List<MessageItem> Messages { get; private set; }

        private void LocalizeObjects(object objectItem, string frmName)
        {
            var controlType = objectItem.GetType();
            var propertyItem = controlType.GetProperty("Name");

            if (propertyItem != null)
            {
                var itemName = propertyItem.GetValue(objectItem);

                var caption = GetGuiCaption(frmName,
                            itemName.ToString(),
                            CultureInfo.CurrentCulture.TwoLetterISOLanguageName);

                if (!String.IsNullOrEmpty(caption))
                {
                    propertyItem = controlType.GetProperty("Text");

                    if (propertyItem != null)
                    {
                        propertyItem.SetValue(objectItem, caption);
                    }
                    else
                    {
                        propertyItem = controlType.GetProperty("Caption");
                        if (propertyItem != null)
                        {
                            propertyItem.SetValue(objectItem, caption);
                        }

                    }
                }


            }
        }

        public string GetGuiCaption(string NameForm, string NameGuiEntry, string languageShort)
        {
            var caption = "";
            var objForms =
                objDBLevel_GuiItems_L1.ObjectRels.Where(form => form.Name_Other.ToLower() == NameForm.ToLower())
                                      .ToList();

            var objGuiEntries = new List<clsObjectRel>();

            if (NameGuiEntry != NameForm)
            {
                objGuiEntries = (from objForm in objForms
                                     join objGuiEntry in objDBLevel_GuiItems_L2.ObjectRels on objForm.ID_Other
                                         equals objGuiEntry.ID_Object
                                     where objGuiEntry.Name_Other.ToLower() == NameGuiEntry.ToLower()
                                     select objGuiEntry).ToList();

                
            }
            else
            {
                objGuiEntries = objForms;
            }

            var objGuiCaptions = (from objGuiEntry in objGuiEntries
                                  join objGuiCaption in objDBLevel_GuiCaption.ObjectRels on objGuiEntry.ID_Other
                                      equals objGuiCaption.ID_Object
                                  join objLanguage in objDBLevel_Languages.ObjectRels on objGuiCaption.ID_Other
                                      equals objLanguage.ID_Object
                                  join objShort in objDBLevel_LanguageShort.ObjAtts on objLanguage.ID_Other
                                      equals objShort.ID_Object
                                  where objShort.Val_String.ToLower() == languageShort.ToLower()
                                  select objGuiCaption.Name_Other).ToList();

            if (objGuiCaptions.Any())
            {
                caption = objGuiCaptions.First();
            }

            return caption;
        }

        public MessageItem GetMessageItem(string idMessage, string languageShort = null)
        {
            if (string.IsNullOrEmpty(languageShort)) languageShort = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            var messageItem = Messages.FirstOrDefault(msg => msg.IdMessage == idMessage && (!string.IsNullOrEmpty(languageShort) ? msg.LanguageShort.ToLower() == languageShort.ToLower() : 1 == 1));
            return messageItem;
        }

        public string GetMessage(string idMessage, string languageShort = null)
        {
            if (string.IsNullOrEmpty(languageShort)) languageShort = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

            var messageItem = Messages.FirstOrDefault(msg => msg.IdMessage == idMessage && (!string.IsNullOrEmpty(languageShort) ? msg.LanguageShort.ToLower() == languageShort.ToLower() : 1 == 1));

            if (messageItem != null)
            {
                return GetErrorMessageOrCaption(messageItem, false);
            }
            else
            {
                return "";
            }

        }

        public string GetCaption(string idMessage, string languageShort = null)
        {
            if (string.IsNullOrEmpty(languageShort)) languageShort = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

            var messageItem = Messages.FirstOrDefault(msg => msg.IdMessage == idMessage && (!string.IsNullOrEmpty(languageShort) ? msg.LanguageShort.ToLower() == languageShort.ToLower() : 1 == 1));

            if (messageItem != null)
            {
                return GetErrorMessageOrCaption(messageItem, true);
            }
            else
            {
                return "";
            }

        }

        private string GetErrorMessageOrCaption(MessageItem messageItem, bool caption)
        {
            if (caption)
            {
                return messageItem != null ? messageItem.Caption : "";
            }
            else
            {
                return messageItem != null ? messageItem.Message : "";
            }
        }

        public string GetToolTip(string NameForm, string NameGuiEntry, string languageShort)
        {
            var toolTip = "";

            return toolTip;
        }

        public string GetUserMessageCaption(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var caption = "";

            return caption;
        }

        public string GetInputMessageCaption(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var caption = "";

            return caption;
        }

        public string GetErrorMessageCaption(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var caption = "";

            return caption;
        }

        public string GetUserMessage(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var message = "";

            return message;
        }

        public string GetInputMessage(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var message = "";

            return message;
        }

        public string GetErrorMessage(string NemeFrom, string NameMessage, string lanugageShort)
        {
            var message = "";

            return message;
        }

        private void Initialize()
        {
            objDBLevel_GuiItems_L1 = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_GuiItems_L2 = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_GuiCaption = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_ToolTips = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_Messages = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_LocalizedMessages = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_Languages = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_LanguageShort = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_Caption = new OntologyModDBConnector(localConfig.Globals);
            objDBLevel_Message = new OntologyModDBConnector(localConfig.Globals);

            if (GetData_001_GuiEntries().GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (GetData_002_GuiCaptions().GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (GetData_003_ToolTipMessages().GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        if (GetData_004_Messages().GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            if (GetData_005_LocalizedMessage().GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                if (GetData_006_Languages().GUID == localConfig.Globals.LState_Success.GUID)
                                {
                                    if (GetData_007_LanguageShort().GUID == localConfig.Globals.LState_Success.GUID)
                                    {
                                        if (GetData_008_CaptionMessage().GUID == localConfig.Globals.LState_Success.GUID)
                                        {

                                        }
                                        else
                                        {
                                            throw new Exception("Data-Error!");
                                        } 
                                    }
                                    else
                                    {
                                        throw new Exception("Data-Error!");
                                    } 
                                }
                                else
                                {
                                    throw new Exception("Data-Error!");
                                } 
                            }
                            else
                            {
                                throw new Exception("Data-Error!");
                            } 
                        }
                        else
                        {
                            throw new Exception("Data-Error!");
                        }   
                    }
                    else
                    {
                        throw new Exception("Data-Error!");
                    }
                }
                else
                {
                    throw new Exception("Data-Error!");
                }
            }
            else
            {
                throw new Exception("Data-Error!");
            }


        }

        private clsOntologyItem GetData_001_GuiEntries()
        {

            var searchGuiEntriesL1 = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Object = localConfig.Id_Development,
                            ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                            ID_Parent_Other = localConfig.OItem_type_gui_entires.GUID
                        }
                };

            var objOItem_Result = objDBLevel_GuiItems_L1.GetDataObjectRel(searchGuiEntriesL1, doIds: false);

            if (objOItem_Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchGuiEntriesL2 = objDBLevel_GuiItems_L1.ObjectRels.Select(guie => new clsObjectRel
                    {
                        ID_Object = guie.ID_Other,
                        ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                        ID_Parent_Other = localConfig.OItem_type_gui_entires.GUID
                    }).ToList();

                if (searchGuiEntriesL2.Any())
                {
                    objOItem_Result = objDBLevel_GuiItems_L2.GetDataObjectRel(searchGuiEntriesL2, doIds: false);    
                }
                


            }

            return objOItem_Result;
        }

        private clsOntologyItem GetData_002_GuiCaptions()
        {
            var searchGuiCaptions = objDBLevel_GuiItems_L1.ObjectRels.Select(guie => new clsObjectRel
                {
                    ID_Object = guie.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                    ID_Parent_Other = localConfig.OItem_type_gui_caption.GUID
                }).ToList();

            searchGuiCaptions.AddRange(objDBLevel_GuiItems_L2.ObjectRels.Select(guie => new clsObjectRel
                {
                    ID_Object = guie.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                    ID_Parent_Other = localConfig.OItem_type_gui_caption.GUID
                }));

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();
            if (searchGuiCaptions.Any())
            {
                objOItem_Result = objDBLevel_GuiCaption.GetDataObjectRel(searchGuiCaptions, doIds: false);    
            }
            

            return objOItem_Result;
        }

        private clsOntologyItem GetData_003_ToolTipMessages()
        {
            var searchToolTips = objDBLevel_GuiItems_L1.ObjectRels.Select(guie => new clsObjectRel
            {
                ID_Object = guie.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                ID_Parent_Other = localConfig.OItem_type_tooltip_messages.GUID
            }).ToList();

            searchToolTips.AddRange(objDBLevel_GuiItems_L2.ObjectRels.Select(guie => new clsObjectRel
                {
                    ID_Object = guie.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_is_defined_by.GUID,
                    ID_Parent_Other = localConfig.OItem_type_tooltip_messages.GUID
                }));

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();
            if (searchToolTips.Any())
            {
                objOItem_Result = objDBLevel_ToolTips.GetDataObjectRel(searchToolTips, doIds: false);    
            }
            

            return objOItem_Result;
        }

        private clsOntologyItem GetData_004_Messages()
        {
            var searchMessages = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Object = localConfig.Id_Development,
                            ID_RelationType = localConfig.OItem_relationtype_user_message.GUID,
                            ID_Parent_Other = localConfig.OItem_class_messages.GUID
                        },
                    new clsObjectRel
                        {
                            ID_Object = localConfig.Id_Development,
                            ID_RelationType = localConfig.OItem_relationtype_inputmessage.GUID,
                            ID_Parent_Other = localConfig.OItem_class_messages.GUID
                        },
                    new clsObjectRel
                        {
                            ID_Object = localConfig.Id_Development,
                            ID_RelationType = localConfig.OItem_relationtype_errormessage.GUID,
                            ID_Parent_Other = localConfig.OItem_class_messages.GUID
                        }
                };

            var objOItem_Result = objDBLevel_Messages.GetDataObjectRel(searchMessages, doIds: false);

            return objOItem_Result;
        }

        private clsOntologyItem GetData_005_LocalizedMessage()
        {
            var searchLocalizedMessages = objDBLevel_Messages.ObjectRels.Select(msg => new clsObjectRel
                {
                    ID_Object = msg.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                    ID_Parent_Other = localConfig.OItem_class_localized_message.GUID
                }).ToList();

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();
            if (searchLocalizedMessages.Any())
            {
                objOItem_Result = objDBLevel_LocalizedMessages.GetDataObjectRel(searchLocalizedMessages,
                                                                                      doIds: false);    
            }
            

            return objOItem_Result;
        }

        private clsOntologyItem GetData_006_Languages()
        {
            var searchLanguages = objDBLevel_GuiCaption.ObjectRels.Select(guie => new clsObjectRel
                {
                    ID_Object = guie.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                    ID_Parent_Other = localConfig.OItem_type_language.GUID
                }).ToList();

            searchLanguages.AddRange(objDBLevel_ToolTips.ObjectRels.Select(toolt => new clsObjectRel
                {
                    ID_Object = toolt.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                    ID_Parent_Other = localConfig.OItem_type_language.GUID
                }));

            searchLanguages.AddRange(objDBLevel_LocalizedMessages.ObjectRels.Select(lmsg => new clsObjectRel
            {
                ID_Object = lmsg.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_iswrittenin.GUID,
                ID_Parent_Other = localConfig.OItem_type_language.GUID
            }));

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();

            if (searchLanguages.Any())
            {
                objOItem_Result = objDBLevel_Languages.GetDataObjectRel(searchLanguages, doIds: false);
            }

            return objOItem_Result;
        }

        private clsOntologyItem GetData_007_LanguageShort()
        {
            var searchLanguageShort = objDBLevel_Languages.ObjectRels.Select(lang => new clsObjectAtt
                {
                    ID_Object = lang.ID_Other,
                    ID_AttributeType = localConfig.OItem_attributetype_short.GUID
                }).ToList();

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();

            if (searchLanguageShort.Any())
            {
                objOItem_Result = objDBLevel_LanguageShort.GetDataObjectAtt(searchLanguageShort, doIds: false);

            }

            return objOItem_Result;
        }

        private clsOntologyItem GetData_008_CaptionMessage()
        {
            var searchCaption = objDBLevel_LocalizedMessages.ObjectRels.Select(lmsg => new clsObjectAtt
                {
                    ID_Object = lmsg.ID_Other,
                    ID_AttributeType = localConfig.OItem_attribute_caption.GUID
                }).ToList();

            var objOItem_Result = localConfig.Globals.LState_Success.Clone();

            if (searchCaption.Any())
            {
                objOItem_Result = objDBLevel_Caption.GetDataObjectAtt(searchCaption, doIds: false);

            }

            if (objOItem_Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchMessage = objDBLevel_LocalizedMessages.ObjectRels.Select(lmsg => new clsObjectAtt
                    {
                        ID_Object = lmsg.ID_Other,
                        ID_AttributeType = localConfig.OItem_attributetype_message.GUID
                    }).ToList();

                if (searchCaption.Any())
                {
                    objOItem_Result = objDBLevel_Message.GetDataObjectAtt(searchMessage, doIds: false);

                }
            }

            if (objOItem_Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                Messages = (from messageItem in objDBLevel_Messages.ObjectRels
                            join localizedMessageItem in objDBLevel_LocalizedMessages.ObjectRels on messageItem.ID_Other equals localizedMessageItem.ID_Object
                            join caption in objDBLevel_Caption.ObjAtts on localizedMessageItem.ID_Other equals caption.ID_Object
                            join message in objDBLevel_Message.ObjAtts on localizedMessageItem.ID_Other equals message.ID_Object
                            join languageItem in objDBLevel_Languages.ObjectRels on localizedMessageItem.ID_Other equals languageItem.ID_Object
                            join languageShortItem in objDBLevel_LanguageShort.ObjAtts on languageItem.ID_Other equals languageShortItem.ID_Object
                            select new MessageItem
                            {
                                IdMessage = messageItem.ID_Other,
                                NameMessage = messageItem.Name_Other,
                                IdLocalizedMessage = localizedMessageItem.ID_Other,
                                NameLocalizedMessage = localizedMessageItem.Name_Other,
                                IdLanguage = languageItem.ID_Other,
                                NameLanguage = languageItem.Name_Other,
                                IdRelationType = messageItem.ID_RelationType,
                                NameRelationType = messageItem.Name_RelationType,
                                LanguageShort = languageShortItem.Val_String,
                                Message = message.Val_String,
                                Caption = caption.Val_String
                            }).ToList();
            }

            return objOItem_Result;
        }
    }
}
