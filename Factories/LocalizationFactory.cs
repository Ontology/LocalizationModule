﻿using LocalizationModule.Models;
using LocalizationModule.Notifications;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LocalizationModule.Factories
{
    public class LocalizationFactory : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private List<clsObjectRel> localizationToRefList;
        private List<clsObjectAtt> localizationToMessageList;
        private List<clsObjectRel> languageOfLocalizationList;

        public List<LocalizationDetail> LocalizationDetailList { get; private set; }

        private Thread createLocalizationDetailAsync;

        private clsOntologyItem resultLocalizationDetail;
        public clsOntologyItem ResultLocalizationDetail
        {
            get
            {
                return resultLocalizationDetail;
            }
            set
            {
                resultLocalizationDetail = value;
                RaisePropertyChanged(NotifyChanges.LocalizationFactory_ResultLocalizationDetail);
            }
        }

        public clsOntologyItem CreateLocalizationDetailList(List<clsObjectRel> localizationToRefList,
            List<clsObjectAtt> localizationToMessageList,
            List<clsObjectRel> languagesOfLocalizationList)
        {
            StopRead();


            this.localizationToRefList = localizationToRefList;
            this.localizationToMessageList = localizationToMessageList;
            this.languageOfLocalizationList = languagesOfLocalizationList;

            createLocalizationDetailAsync = new Thread(CreateLocalizationDetailListAsync);
            createLocalizationDetailAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void CreateLocalizationDetailListAsync()
        {
            LocalizationDetailList = (from objLocalization in localizationToRefList
                                      join objMessage in localizationToMessageList  on objLocalization.ID_Object equals objMessage.ID_Object into objMessages
                                      from objMessage in objMessages.DefaultIfEmpty()
                                      join objLanguage in languageOfLocalizationList  on objLocalization.ID_Object equals objLanguage.ID_Object
                                      select new LocalizationDetail
                                      {
                                          ID_Localization = objLocalization.ID_Object,
                                          Name_Localization = objLocalization.Name_Object,
                                          ID_Parent_Localization = objLocalization.ID_Parent_Object,
                                          ID_Language = objLanguage.ID_Other,
                                          Name_Language = objLanguage.Name_Other,
                                          ID_Attribute_Message = objMessage != null ? objMessage.ID_Attribute : null,
                                          Message = objMessage != null ? objMessage.Val_String : null
                                      }).ToList();

            ResultLocalizationDetail = localConfig.Globals.LState_Success.Clone();
        }


        public LocalizationFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }

        public void StopRead()
        {
            if (createLocalizationDetailAsync != null)
            {
                try
                {
                    createLocalizationDetailAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
