﻿using LocalizationModule.Factories;
using LocalizationModule.Models;
using LocalizationModule.Services;
using LocalizationModule.Translations;
using LoggingServiceController;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LocalizationModule.Controllers
{
    public class LocalizationTreeController : LocalizationTreeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;
        private ElasticServiceAgent elasticServiceAgent;
        private LocalizationFactory localizationFactory;

        private LoggingManager loggingManager;

        private TranslationController translationController = new TranslationController();

        private clsOntologyItem referenceItem;

        private clsLocalConfig localConfig;

        private ViewTreeNode treeNodeRoot;
        private ViewTreeNode treeNodeNames;
        private ViewTreeNode treeNodeDescriptions;
        private ViewTreeNode treeNodeNameStandard;
        private ViewTreeNode treeNodeNameAdditional;
        private ViewTreeNode treeNodeDescriptionStandard;
        private ViewTreeNode treeNodeDescriptionAdditional;

        private TreeFactory treeFactory;
        private SessionFile treeSessionFile;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public LocalizationTreeController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += LocalizationTreeController_PropertyChanged;
        }

        private void LocalizationTreeController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewController_ViewTreeNodes)
            {
                
            }
            else
            {
                var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

                if (property == null) return;

                property.ViewItem.AddValue(property.Property.GetValue(this));

                webSocketServiceAgent.SendPropertyChange(e.PropertyName);
            }
            

            if (e.PropertyName == Notifications.NotifyChanges.Navigation_Text_SelectedNodeId)
            {
                if (Text_SelectedNodeId.StartsWith(treeNodeNames.Id) && !Text_SelectedNodeId.StartsWith(treeNodeNameAdditional.Id))
                {
                    var languageId = Text_SelectedNodeId.Replace(treeNodeNames.Id, "");
                    var oItemLanguage = elasticServiceAgent.GetOItem(languageId, localConfig.Globals.Type_Object);
                    var translationItem = new LanguageItem
                    {
                        IdLanguage = oItemLanguage.GUID,
                        NameLanguage = oItemLanguage.Name,
                        IdNodeType = localConfig.OItem_type_localized_names.GUID
                    };

                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_SelectedLanguage,
                        GenericParameterItems = new List<object>
                        {
                            translationItem
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
                else if (Text_SelectedNodeId.StartsWith(treeNodeDescriptions.Id) && !Text_SelectedNodeId.StartsWith(treeNodeDescriptionAdditional.Id))
                {
                    var languageId = Text_SelectedNodeId.Replace(treeNodeDescriptions.Id, "");
                    var oItemLanguage = elasticServiceAgent.GetOItem(languageId, localConfig.Globals.Type_Object);
                    var translationItem = new LanguageItem
                    {
                        IdLanguage = oItemLanguage.GUID,
                        NameLanguage = oItemLanguage.Name,
                        IdNodeType = localConfig.OItem_type_localizeddescription.GUID
                    };

                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_SelectedLanguage,
                        GenericParameterItems = new List<object>
                        {
                            translationItem
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
                else if (Text_SelectedNodeId.StartsWith(treeNodeNameAdditional.Id))
                {
                    var languageId = Text_SelectedNodeId.Replace(treeNodeNameAdditional.Id, "");
                    var oItemLanguage = elasticServiceAgent.GetOItem(languageId, localConfig.Globals.Type_Object);
                    var translationItem = new LanguageItem
                    {
                        IdLanguage = oItemLanguage.GUID,
                        NameLanguage = oItemLanguage.Name,
                        IdNodeType = localConfig.OItem_type_localized_names.GUID
                    };

                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_SelectedLanguage,
                        GenericParameterItems = new List<object>
                        {
                            translationItem
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
                else if (Text_SelectedNodeId.StartsWith(treeNodeDescriptionAdditional.Id))
                {
                    var languageId = Text_SelectedNodeId.Replace(treeNodeDescriptionAdditional.Id, "");
                    var oItemLanguage = elasticServiceAgent.GetOItem(languageId, localConfig.Globals.Type_Object);
                    var translationItem = new LanguageItem
                    {
                        IdLanguage = oItemLanguage.GUID,
                        NameLanguage = oItemLanguage.Name,
                        IdNodeType = localConfig.OItem_type_localizeddescription.GUID
                    };

                    var interModMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_SelectedLanguage,
                        GenericParameterItems = new List<object>
                        {
                            translationItem
                        }
                    };

                    webSocketServiceAgent.SendInterModMessage(interModMessage);
                }
            }
        }

        

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;
            treeFactory = new TreeFactory(localConfig.Globals);
            treeFactory.PropertyChanged += TreeFactory_PropertyChanged;

            loggingManager = new LoggingManager(localConfig.Globals);
            loggingManager.PropertyChanged += LoggingManager_PropertyChanged;

            localizationFactory = new LocalizationFactory(localConfig);
            localizationFactory.PropertyChanged += LocalizationFactory_PropertyChanged;

            CreateBaseTree();
        }

        private void StateMachine_closedSocket()
        {
            elasticServiceAgent.StopRead();
            localizationFactory.StopRead();
            treeFactory.StopFactory();
            webSocketServiceAgent.RemoveAllResources();
            elasticServiceAgent = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


        }

        private void StateMachine_loginSucceded()
        {
            IsToggled_Listen = true;
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SendModel();

            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            treeSessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");


            var result = treeFactory.WriteJson(ViewTreeNodes, treeSessionFile);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {

            }
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {

        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void LocalizationFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.LocalizationFactory_ResultLocalizationDetail)
            {
                if (localizationFactory.ResultLocalizationDetail.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var localizationDetailList = localizationFactory.LocalizationDetailList;

                    localizationDetailList.ForEach(detail =>
                    {
                        if (detail.ID_Parent_Localization == localConfig.OItem_type_localized_names.GUID)
                        {
                            if (treeNodeNameStandard.Id.Replace(treeNodeNameStandard.ParentNode.Id, "") == detail.ID_Language)
                            {
                                treeNodeNameStandard.GenerateHtml(detail.Name_Language, size:2);
                                TreeViewNode_TreeViewNodeToChange = new ViewTreeChangeNode
                                {
                                    Id = treeNodeNameStandard.Id,
                                    NewLabel = treeNodeNameStandard.Html
                                };
                                
                            }
                            else
                            {
                                treeNodeNameAdditional.SubNodes.ForEach(nodeItem =>
                                {
                                    if (nodeItem.Id.Replace(nodeItem.ParentNode.Id, "") == detail.ID_Language)
                                    {
                                        nodeItem.GenerateHtml(detail.Name_Language, size: 2);
                                        TreeViewNode_TreeViewNodeToChange = new ViewTreeChangeNode
                                        {
                                            Id = nodeItem.Id,
                                            NewLabel = nodeItem.Html
                                        };
                                        
                                    }
                                });
                                
                            }
                        }
                        else if (detail.ID_Parent_Localization == localConfig.OItem_type_localizeddescription.GUID)
                        {
                            if (treeNodeDescriptionStandard.Id.Replace(treeNodeDescriptionStandard.ParentNode.Id, "") == detail.ID_Language)
                            {
                                treeNodeDescriptionStandard.GenerateHtml(detail.Name_Language, size: 2);
                                TreeViewNode_TreeViewNodeToChange = new ViewTreeChangeNode
                                {
                                    Id = treeNodeDescriptionStandard.Id,
                                    NewLabel = treeNodeDescriptionStandard.Html
                                };
                                
                            }
                            else
                            {
                                treeNodeDescriptionAdditional.SubNodes.ForEach(nodeItem =>
                                {
                                    if (nodeItem.Id.Replace(nodeItem.ParentNode.Id, "") == detail.ID_Language)
                                    {
                                        nodeItem.GenerateHtml(detail.Name_Language, size: 2);
                                        TreeViewNode_TreeViewNodeToChange = new ViewTreeChangeNode
                                        {
                                            Id = nodeItem.Id,
                                            NewLabel = nodeItem.Html
                                        };
                                    }
                                });
                            }
                        }
                    });
                }
            }
        }

        private void TreeFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == treeFactory.NotifyResult)
            {
                if (treeFactory.ResultJson.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Resource_TreeData = treeSessionFile.FileUri.AbsoluteUri;
                }
            }
        }

        private void CreateBaseTree()
        {
            ViewTreeNodes = new List<ViewTreeNode>();

            treeNodeRoot = new ViewTreeNode
            {
                Id = localConfig.Globals.Root.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Root,
                IsExpanded = true
            };

            treeNodeRoot.GenerateHtml(localConfig.Globals.Root.Name);

            treeNodeNames = new ViewTreeNode
            {
                Id = localConfig.OItem_type_localized_names.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Name,
                ParentNode = treeNodeRoot,
                IsExpanded = true
            };

            treeNodeNames.GenerateHtml(localConfig.OItem_type_localized_names.Name);

            treeNodeDescriptions = new ViewTreeNode
            {
                Id = localConfig.OItem_type_localizeddescription.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Description,
                ParentNode = treeNodeRoot,
                IsExpanded = true
            };

            treeNodeDescriptions.GenerateHtml(localConfig.OItem_type_localizeddescription.Name);

            treeNodeRoot.SubNodes = new List<ViewTreeNode>
            {
                treeNodeNames,
                treeNodeDescriptions
            };

            treeNodeDescriptionStandard = new ViewTreeNode
            {
                Id = treeNodeDescriptions.Id + localConfig.OItem_StandardLanguage.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Standard,
                ParentNode = treeNodeDescriptions,
                IsExpanded = true
            };

            treeNodeDescriptionStandard.GenerateHtml(localConfig.OItem_StandardLanguage.Name);

            treeNodeDescriptionAdditional = new ViewTreeNode
            {
                Id = localConfig.OItem_relationtype_additional.GUID + localConfig.OItem_type_localizeddescription.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Additional,
                ParentNode = treeNodeDescriptions,
                IsExpanded = true
            };

            treeNodeDescriptionAdditional.GenerateHtml(localConfig.OItem_relationtype_additional.Name);

            treeNodeNameStandard = new ViewTreeNode
            {
                Id = treeNodeNames.Id + localConfig.OItem_StandardLanguage.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Standard,
                ParentNode = treeNodeNames,
                IsExpanded = true
            };

            treeNodeNameStandard.GenerateHtml(localConfig.OItem_StandardLanguage.Name);

            treeNodeNameAdditional = new ViewTreeNode
            {
                Id = localConfig.OItem_relationtype_additional.GUID + localConfig.OItem_type_localized_names.GUID,
                NameNodeImage = Notifications.NotifyChanges.NodeIcon_Additional,
                ParentNode = treeNodeNames,
                IsExpanded = true
            };

            treeNodeNameAdditional.GenerateHtml(localConfig.OItem_relationtype_additional.Name);

            localConfig.OList_Languages.ForEach(lang =>
            {
                var addNodeNameAdditional = new ViewTreeNode
                {
                    Id = treeNodeNameAdditional.Id + lang.GUID,
                    NameNodeImage = Notifications.NotifyChanges.NodeIcon_Standard,
                    ParentNode = treeNodeNameAdditional
                };

                addNodeNameAdditional.GenerateHtml(lang.Name);

                if (treeNodeNameAdditional.SubNodes == null)
                {
                    treeNodeNameAdditional.SubNodes = new List<ViewTreeNode>();
                }

                treeNodeNameAdditional.SubNodes.Add(addNodeNameAdditional);

                var addNodeDescAdditional = new ViewTreeNode
                {
                    Id = treeNodeDescriptionAdditional.Id + lang.GUID,
                    NameNodeImage = Notifications.NotifyChanges.NodeIcon_Standard,
                    ParentNode = treeNodeDescriptionAdditional
                };

                addNodeDescAdditional.GenerateHtml(lang.Name);

                if (treeNodeDescriptionAdditional.SubNodes == null)
                {
                    treeNodeDescriptionAdditional.SubNodes = new List<ViewTreeNode>();
                }

                treeNodeDescriptionAdditional.SubNodes.Add(addNodeDescAdditional);
            });

            treeNodeDescriptions.SubNodes = new List<ViewTreeNode>
            {
                treeNodeDescriptionStandard,
                treeNodeDescriptionAdditional
            };

            treeNodeNames.SubNodes = new List<ViewTreeNode>
            {
                treeNodeNameStandard,
                treeNodeNameAdditional
            };

            var result = elasticServiceAgent.GetStandardLanguages();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                
                ViewTreeNodes = new List<ViewTreeNode>
                {
                    treeNodeRoot
                };
            }
            

        }

        private void LoggingManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticServiceAgent_ResultLocalizedData)
            {
                if (elasticServiceAgent.ResultLocalizedData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var result = localizationFactory.CreateLocalizationDetailList(elasticServiceAgent.LocalizedToRefList,
                        elasticServiceAgent.MessagesOfLocalizationList,
                        elasticServiceAgent.LanguagesOfLoclizationList);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }


        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                
                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;
               
                referenceItem = objectItem;

                CreateBaseTree();
                treeSessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");


                var result = treeFactory.WriteJson(ViewTreeNodes, treeSessionFile);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {

                }
                elasticServiceAgent.GetLocalizedData(referenceItem);
            }

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
