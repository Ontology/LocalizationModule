﻿using LocalizationModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Controllers
{
    public class LocalizationTreeViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
        public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string resource_TreeData;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Url, ViewItemId = "treeData", ViewItemType = ViewItemType.JsonUrl)]
        public string Resource_TreeData
        {
            get { return resource_TreeData; }
            set
            {

                resource_TreeData = value;

                RaisePropertyChanged(NotifyChanges.ViewController_Resource_TreeData);

            }
        }

        private string text_SelectedNodeId;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "selectedNodeId", ViewItemType = ViewItemType.Content)]
        public string Text_SelectedNodeId
        {
            get { return text_SelectedNodeId; }
            set
            {
                if (text_SelectedNodeId == value) return;

                text_SelectedNodeId = value;

                RaisePropertyChanged(NotifyChanges.Navigation_Text_SelectedNodeId);

            }
        }

        private ViewTreeChangeNode treeviewnode_TreeViewNodeToChange;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TreeNode, ViewItemId = "treeData", ViewItemType = ViewItemType.Change)]
        public ViewTreeChangeNode TreeViewNode_TreeViewNodeToChange
        {
            get { return treeviewnode_TreeViewNodeToChange; }
            set
            {

                treeviewnode_TreeViewNodeToChange = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_TreeViewNode_TreeViewNodeToChange);

            }
        }

        private List<ViewTreeNode> viewTreeNodes;
        public List<ViewTreeNode> ViewTreeNodes
        {
            get { return viewTreeNodes; }
            set
            {
                viewTreeNodes = value;
                RaisePropertyChanged(NotifyChanges.ViewController_ViewTreeNodes);
            }
        }
    }
}
