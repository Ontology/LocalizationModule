﻿using LocalizationModule.Factories;
using LocalizationModule.Models;
using LocalizationModule.Services;
using LocalizationModule.Translations;
using LoggingServiceController;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.ComponentModel;

namespace LocalizationModule.Controllers
{
    public class LocalizedDescriptionController : LocalizedDescriptionViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;
        private ElasticServiceAgent elasticServiceAgent;

        private LoggingManager loggingManager;

        private LocalizationFactory localizationFactory;

        private TranslationController translationController = new TranslationController();

        private clsOntologyItem referenceItem;
        private LanguageItem languageItem;

        private LocalizationDetail localizationDetail;

        private clsLocalConfig localConfig;

        private Timer timer;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public LocalizedDescriptionController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += LocalizationTreeController_PropertyChanged;
        }

        private void LocalizationTreeController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));


            webSocketServiceAgent.SendPropertyChange(e.PropertyName);


        }



        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            elasticServiceAgent = new ElasticServiceAgent(localConfig);
            elasticServiceAgent.PropertyChanged += ElasticServiceAgent_PropertyChanged;

            loggingManager = new LoggingManager(localConfig.Globals);
            loggingManager.PropertyChanged += LoggingManager_PropertyChanged;

            localizationFactory = new LocalizationFactory(localConfig);
            localizationFactory.PropertyChanged += LocalizationFActory_PropertyChanged;

            timer = new Timer();
            timer.Interval = 300;
            timer.Elapsed += Timer_Elapsed;
        }

        private void StateMachine_closedSocket()
        {
            elasticServiceAgent.StopRead();
            webSocketServiceAgent.RemoveAllResources();
            elasticServiceAgent = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Notifications.NotifyChanges.Channel_SelectedLanguage,
                EndpointType = EndpointType.Receiver
            });

            
            IsToggled_Listen = true;
            webSocketServiceAgent.SetVisibility(true);
            webSocketServiceAgent.SetEnable(false);
            webSocketServiceAgent.SendModel();

            Label_ItemIdentificationLabel = translationController.Label_ItemIdentification;
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
         
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();

            
        }

        private void LocalizationFActory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.LocalizationFactory_ResultLocalizationDetail)
            {
                if (localizationFactory.ResultLocalizationDetail.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var localizationData = localizationFactory.LocalizationDetailList;

                    if (localizationData.Any())
                    {
                        localizationDetail = localizationData.First();

                        Text_LocalizedDescription = localizationDetail.Message;
                    }
                    else
                    {
                        localizationDetail = null;
                    }
                }
            }
        }

        private void LoggingManager_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

        }

        private void ElasticServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticServiceAgent_ResultLocalizedData)
            {
                if (elasticServiceAgent.ResultLocalizedData.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    localizationFactory.CreateLocalizationDetailList(elasticServiceAgent.LocalizedToRefList, elasticServiceAgent.MessagesOfLocalizationList, elasticServiceAgent.LanguagesOfLoclizationList);

                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

      
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {

                Initialize();
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {



            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changeItem =>
                {
                    if (changeItem.ViewItemId == "localizedDescription")
                    {
                        timer.Stop();
                        timer.Start();
                    };
                });
            }


        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
                var objectItem = message.OItems.LastOrDefault();

                if (objectItem == null) return;

                if (referenceItem != null && referenceItem.GUID == objectItem.GUID) return;

                referenceItem = objectItem;

                TestReferences();

            }
            else if (message.ChannelId == Notifications.NotifyChanges.Channel_SelectedLanguage)
            {
                var language = message.GenericParameterItems.LastOrDefault();
                if (language == null) return;

                var languageCheck = Newtonsoft.Json.JsonConvert.DeserializeObject<LanguageItem>(language.ToString());
                if (languageItem != null && languageItem.IdLanguage == languageCheck.IdLanguage) return;
                if (languageCheck.IdNodeType != localConfig.OItem_type_localizeddescription.GUID) return;

                languageItem = languageCheck;

                TestReferences();
            }

        }

        private void TestReferences()
        {
            if (referenceItem != null && languageItem != null)
            {
                if (referenceItem.Type == localConfig.Globals.Type_Object)
                {
                    var relatedClass = elasticServiceAgent.GetOItem(referenceItem.GUID_Parent, localConfig.Globals.Type_Class);
                    Label_ItemIdentification = System.Web.HttpUtility.HtmlEncode(relatedClass.Name) + @"\" + System.Web.HttpUtility.HtmlEncode(referenceItem.Name);
                }
                else
                {
                    Label_ItemIdentification = System.Web.HttpUtility.HtmlEncode(referenceItem.Name);
                }
                
                elasticServiceAgent.GetLocalizedDescriptionOfReference(referenceItem, languageItem);
            }
        }

     
        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
