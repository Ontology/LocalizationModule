﻿using LocalizationModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Controllers
{
    public class LocalizedDescriptionViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
        public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string label_ItemIdentification;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ItemIdentification, ViewItemId = "@classItem@", ViewItemType = ViewItemType.Content)]
        public string Label_ItemIdentification
        {
            get { return label_ItemIdentification; }
            set
            {
                if (label_ItemIdentification == value) return;

                label_ItemIdentification = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ItemIdentification);

            }
        }

        private string label_ItemIdentificationLabel;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "itemIdentificationLabel", ViewItemType = ViewItemType.Content)]
        public string Label_ItemIdentificationLabel
        {
            get { return label_ItemIdentificationLabel; }
            set
            {
                if (label_ItemIdentificationLabel == value) return;

                label_ItemIdentificationLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_ItemIdentificationLabel);

            }
        }

        private string text_LocalizedDescription;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.TextArea, ViewItemId = "localizedDescription", ViewItemType = ViewItemType.Content)]
        public string Text_LocalizedDescription
        {
            get { return text_LocalizedDescription; }
            set
            {
                if (text_LocalizedDescription == value) return;

                text_LocalizedDescription = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_LocalizedDescription);

            }
        }
    }
}
